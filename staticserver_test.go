package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
)

var testContents = []byte("this is a test")

func TestStaticServer(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ioutil.WriteFile(filepath.Join(dir, "test.txt"), testContents, 0600)

	srv := httptest.NewServer(buildStaticServer(dir, "", nil))
	defer srv.Close()

	resp, err := http.Get(srv.URL + "/test.txt")
	if err != nil {
		t.Fatalf("Get() error: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("got status %d, expected 200", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Read() error: %v", err)
	}
	if !bytes.Equal(data, testContents) {
		t.Fatalf("unexpected response '%s'", string(data))
	}
}
