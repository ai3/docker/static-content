FROM docker.io/library/golang:1.21.0 as build
ADD . /src
RUN cd /src && go build -tags netgo -o staticserver ./staticserver.go && strip staticserver

FROM scratch
COPY --from=build /src/staticserver /staticserver
CMD ["/staticserver"]

